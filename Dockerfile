FROM golang:1.17-alpine3.14 as build
WORKDIR /go/src/gitlab.com/agrando/agr-anonymize
RUN apk add --quiet --no-progress --no-cache gcc musl-dev upx git
RUN 
RUN go build -a -ldflags '-linkmode external -extldflags "-static" -w' -o /bin/lamp && \
    upx -q -9 /bin/lamp


FROM busybox:1.35.0-musl
COPY --from=build /bin/lamp /bin/lamp
RUN addgroup -S app
RUN adduser -S app
USER app
